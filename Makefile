prefix=/usr/local
exec_prefix=${prefix}
CONFDIR=${prefix}/etc/fastmake

all: fastmakeV0 fastmakeV1 fastmake


fastmakeV0: fastmakeV0.c
	gcc -o fastmake fastmakeV0.c

fastmakeV1: fastmakeV0
	./fastmake fastmake.c

removeV0: fastmakeV1
	rm fastmakeV0

fastmake: fastmakeV1 removeV0
	./fastmake fastmake.c

install: cleanbin
	if [ ! -d $(CONFDIR) ]; then mkdir -p $(CONFDIR); fi
	if [ -e fastmake ]; then cp fastmake ${exec_prefix}/bin/fastmake; fi

cleanbin:
	rm -f *.o
	rm -f fastmakeV0

clean: cleanbin
	rm -f fastmake

