/* 
 * File:   fastmake.c
 * Author: gandhi
 *
 * Created on 1. September 2013, 17:13
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <dirent.h>

const char searchFileEnding[]=".c";
const int searchFileEndingLen=sizeof(searchFileEnding)-1;

const char searchStartDir[]=".";

const char searchFunctionName[]="main";
const int searchFunctionNameLen=sizeof(searchFunctionName)-1;

const char* allowedMainFormats[]={ "intmain(intargc,char**argv)",
                                   "intmain(void)",
                                   "intmain()",
                                   "voidmain(void)",
                                   "voidmain()" };
const int allowedMainFormatsCount=sizeof(allowedMainFormats)/sizeof(char*);

const char searchIncludeName[]="#include";
const char specificIncludeName[]="#include\"";

const char compileTool[]="gcc";
const char compileToolParameter[]="-Os -g -Wall -Wextra -c";

const char linkerTool[]="gcc";
const char linkerToolParameter[]="--static -o";

typedef struct fileTreeItem_t fileTreeItem_t;

struct fileTreeItem_t{
  char* hFile;
  char* cFile;
  char* oFile;
};

fileTreeItem_t* compileFiles=NULL;
int compileFilesCount=0;
int compileFilesCapacity=0;

char* strdup(const char* src){
  int len=strlen(src)+1;
  char* ret=(char*)malloc(len);
  memcpy(ret, src, len);
  return ret;
}

fileTreeItem_t* findHFile(const char* filename){
  char* nameStart=strrchr(filename, '/');
  int nameLen, n, itemLen;
  
  if(nameStart==NULL){
    nameStart=strrchr(filename, '\\');
  }
  
  if(nameStart==NULL){
    nameStart=(char*)filename;
  }else{
    nameStart=&nameStart[1];
  }
  nameLen=strlen(nameStart);
  
  for(n=0; n<compileFilesCount; n++){
    if(compileFiles[n].hFile!=NULL){
      itemLen=strlen(compileFiles[n].hFile);
      if(nameLen<=itemLen){
	if(0==strcmp(nameStart, &compileFiles[n].hFile[itemLen-nameLen])){
	  return &compileFiles[n];
	}
      }
    }
  }
  return NULL;
}

fileTreeItem_t* addFile(const char* hFilename, const char* cFilename){
  fileTreeItem_t* ret;
  
  if(compileFilesCount==compileFilesCapacity){
    compileFilesCapacity+=16;
    compileFiles=(fileTreeItem_t*)realloc(compileFiles, compileFilesCapacity*sizeof(fileTreeItem_t));
    
    if(compileFiles==NULL){
      fprintf(stderr,"MemoryAllocation failed!");
      exit(-1);
    }
    
    memset(&compileFiles[compileFilesCapacity-16], 0, 16*sizeof(fileTreeItem_t));
  }
  
  if(hFilename!=NULL){
    ret=findHFile(hFilename);
    if(ret!=NULL){
      if(cFilename!=NULL){
	if(ret->cFile!=NULL){
	  free(ret->cFile);
	}
	ret->cFile=strdup(cFilename);
      }
      return ret;
    }
  }
  
  if(compileFiles[compileFilesCount].hFile!=NULL){
    free(compileFiles[compileFilesCount].hFile);
    compileFiles[compileFilesCount].hFile=NULL;
  }
  
  if(hFilename!=NULL){
    compileFiles[compileFilesCount].hFile=strdup(hFilename);
  }
  
  if(compileFiles[compileFilesCount].cFile!=NULL){
    free(compileFiles[compileFilesCount].cFile);
    compileFiles[compileFilesCount].cFile=NULL;
  }
  
  if(cFilename!=NULL){
    compileFiles[compileFilesCount].cFile=strdup(cFilename);
  }
  
  if(compileFiles[compileFilesCount].oFile!=NULL){
    free(compileFiles[compileFilesCount].oFile);
    compileFiles[compileFilesCount].oFile=NULL;
  }
  
  compileFilesCount++;
  return &compileFiles[compileFilesCount-1];
}

void clearList(void){
  compileFilesCount=0;
}

void freeList(void){
  int n;
  
  clearList();
  
  for(n=0;n<compileFilesCapacity;n++){
    if(compileFiles[n].cFile!=NULL){
      free(compileFiles[n].cFile);
    }
    if(compileFiles[n].hFile!=NULL){
      free(compileFiles[n].hFile);
    }
    if(compileFiles[n].oFile!=NULL){
      free(compileFiles[n].oFile);
    }
  }
  
  compileFilesCapacity=0;
  free(compileFiles);
  compileFiles=NULL;
}

void strRemoveSpaces(char* str){
  int n;
 
  for(n=0;n<strlen(str);n++){
    if(str[n] == ' '){
      memmove(&str[n], &str[n+1], strlen(str)-n);
      n--;
    }
  }
}

int checkForMain(const char* filename){
  FILE* inFile;
  int ret=-1;
  int lineCount=0;
  int n;
  char* line = NULL;
  size_t len = 0;
  ssize_t read;
  
  inFile = fopen (filename,"r");
  if (inFile==NULL)
  {
    return ret;
  }
  
  while (((read = getline(&line, &len, inFile)) != -1) && ret < 0) {
    if(NULL != strstr(line, searchFunctionName)){
      strRemoveSpaces(line);
      
      for(n=0;n<allowedMainFormatsCount;n++){
	if(0 == strncmp(allowedMainFormats[n], line, strlen(allowedMainFormats[n]))){
	  ret=lineCount;
	  printf("found %s on line %d in file %s: %s", searchFunctionName, lineCount, filename, line);
	}
      }
    }
    lineCount++;
  }

  free(line);
  fclose (inFile);
  
  return ret;
}

char* findCFile(const char* hFilename){
  FILE* exist;
  char* cFile=strdup(hFilename);
  char* fileEnding=strrchr(cFile,'.');
  
  if(fileEnding==NULL){
    free(cFile);
    return NULL;
  }
  
  fileEnding[1]='c';
  fileEnding[2]='\0';
  
  exist=fopen(cFile,"r");
  if(exist==NULL){
    free(cFile);
    return NULL;
  }
  
  fclose(exist);
  return cFile;
}

void searchDependencies(const char* filename){
  FILE* inFile;
  char* line = NULL;
  char* filePointer;
  char* fileEnd;
  size_t len = 0;
  ssize_t read;
  fileTreeItem_t* currentFile;
  
  inFile = fopen (filename,"r");
  if (inFile==NULL)
  {
    return;
  }
  
  while ((read = getline(&line, &len, inFile)) != -1) {
    if(NULL != strstr(line, searchIncludeName)){
      strRemoveSpaces(line);
      if(NULL != strstr(line, specificIncludeName)){
	filePointer=strchr(line, '\"');
	if(filePointer!=NULL){
	  filePointer=&filePointer[1];
	  fileEnd=strchr(filePointer, '\"');
	  if(fileEnd!=NULL){
	    fileEnd[0]='\0';
	  }
	  printf("found Dependency %s in File %s", filePointer, filename);
	  currentFile=addFile(filePointer, NULL);
	  
	  if(currentFile->cFile==NULL){
	    currentFile->cFile=findCFile(filePointer);
	    if(currentFile->cFile==NULL){
	      printf(" <- warning: no c-File found\n");
	    }else{
	      printf("\n");
	      searchDependencies(currentFile->cFile);
	    }
	  }else{
	    printf(" <- already added\n");
	  }
	}
      }
    }
  }

  free(line);
  fclose (inFile);
}

char* stradd(char* destination, int* destLen, const char* source){
  int destStrLen=0;
  int sourceLen=0;
  
  if(destination!=NULL){
    destStrLen=strlen(destination);
  }
  if(source!=NULL){
    sourceLen=strlen(source);
  }
  
  if((destStrLen+sourceLen+2)>*destLen){
    destination=(char*)realloc(destination, destStrLen+sourceLen+2);
    *destLen=destStrLen+sourceLen+2;
    if(destination==NULL){
      printf("MemoryAllocation Error!\n");
      exit(-1);
    }
  }
  
  if(destStrLen!=0){
    strcat(destination, " ");
  }else{
    destination[0]='\0';
  }
  if(sourceLen!=0){
    strcat(destination, source);
  }
  
  return destination;
}

void compileAll(void){
  int n;
  char* oFile=NULL;
  char* fileEnding=NULL;
  char* cFile=NULL;
  char* command=NULL;
  int commandLen=0;
  
  for(n=0;n<compileFilesCount;n++){
    if(compileFiles[n].cFile!=NULL){
      cFile=strrchr(compileFiles[n].cFile, '/');
      if(cFile==NULL){
	cFile=strrchr(compileFiles[n].cFile, '\\');
      }
      if(cFile==NULL){
	cFile=compileFiles[n].cFile;
      }else{
	cFile=&cFile[1];
      }
      oFile=strdup(cFile);
      fileEnding=strrchr(oFile, '.');
      if(fileEnding!=NULL){
	fileEnding[1]='o';
	fileEnding[2]='\0';
	
	compileFiles[n].oFile=oFile;
	fileEnding=NULL;
	oFile=NULL;
	
	command=stradd(command, &commandLen, compileTool);
	command=stradd(command, &commandLen, compileToolParameter);
	command=stradd(command, &commandLen, compileFiles[n].cFile);
	
	printf("Executing: %s\n", command);
	system(command);
	command[0]='\0';
      }
    }
  }
  free(command);
}

void linkApplication(void){
  int n;
  char* command=NULL;
  int commandLen=0;
  
  command=stradd(command, &commandLen, linkerTool);
  command=stradd(command, &commandLen, linkerToolParameter);
  
  if(compileFilesCount>0){
    char* outFile=strdup(compileFiles[0].cFile);
    char* ending=strrchr(outFile, '.');
    ending[0]='\0';
    command=stradd(command, &commandLen, outFile);
    free(outFile);
  }
  
  for(n=0;n<compileFilesCount;n++){
    if(compileFiles[n].oFile!=NULL){
      command=stradd(command, &commandLen, compileFiles[n].oFile);
    }
  }
  
  printf("Linking application: %s\n", command);
  system(command);
  
  free(command);
}

void buildFile(const char* filename){
  addFile(NULL, filename);
  
  searchDependencies(filename);
  
  compileAll();
  
  linkApplication();
  
  clearList();
}

/*
 * 
 */
int main(int argc, char** argv) {
  DIR* maindir;
  struct dirent* dirFile;
  int fileNameLen;
  
  maindir=opendir(searchStartDir);
  
  if (maindir)
  {
    while ((dirFile = readdir(maindir)) != NULL)
    {
      fileNameLen=strlen(dirFile->d_name);
      if(0==strcmp(&dirFile->d_name[fileNameLen-searchFileEndingLen],searchFileEnding)){
	printf("searching for main() in %s\n", dirFile->d_name);
	if(0<=checkForMain(dirFile->d_name)){
	  buildFile(dirFile->d_name);
	  printf("\n");
	}
      }
    }

    closedir(maindir);
  }
  
  return (EXIT_SUCCESS);
}

