/*******************************************************************************
 *  Copyright (c) 2012 Supercomputing Systems AG
 *  All rights reserved.
 *******************************************************************************/

#ifndef ARGEVAL_H
#define	ARGEVAL_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct
{
  int32_t Identifier;     // All option with the same Identifier are influencing the same Variable
  int32_t ParameterCount; // This specifies how much parameters are needed for this option: "-i %filename" means that '-i' is the option and needs 1 Parameter ('%filename')
                          //  an other Example would be: "--dimension %width %height" -> the option '--dimension' needs 2 Parameters ('%width' and '%height')
  char* OptionString;     // Array with all the valid option strings
  char* Description;      // This string will be shown if the help is being shown or something goes wrong this string does not need to be specified for every optionString
} OptionDefinition_t;

void argEval_registerOptionArray(const OptionDefinition_t* optionArray, const size_t size);
void argEval_outputHelp(FILE* outputStream);
int32_t argEval_Parse(int argc, char** argv, FILE* errorOut);
int32_t argEval_hasOptionBeenSet(const int32_t Identifier);
int32_t argEval_giveOptionParameterCount(const int32_t Identifier);
char** argEval_giveOptionParameters(const int32_t Identifier);
int32_t argEval_giveLooseParameterCount(void);
char** argEval_giveLooseParameters(void);
OptionDefinition_t* argEval_convertStringToOption(const char* arg);
#endif	/* ARGEVAL_H */

/* Exampl: */
/*
typedef enum
{
  showHelp,
  referenceFile,
  enableDetails
} optionEnumeration;

OptionDefinition_t optionArray[] = {
  {showHelp, 0, "-h", NULL},
  {showHelp, 0, "--help", " --help, -h                             Shows the help(this screen)"},
  {referenceFile, 1, "-rf", NULL},
  {referenceFile, 1, "--refFile", " --refFile %fileName%, -rf %fileName%   Specifies the reference File (if the reference is not specified, the first file will be the reference file)"},
  {enableDetails, 0, "-d", NULL},
  {enableDetails, 0, "--details", " --details, -d                          Makes the Program show every difference in detail"}
};

int main(int argc, char** argv)
{
  argEval_registerOptionArray(optionArray, sizeof (optionArray) / sizeof (OptionDefinition_t));
  argEval_Parse(argc, argv, stderr);

  if (0 != argEval_hasOptionBeenSet(showHelp)) {
    fprintf(stdout, "SCS Camera Viewer V1.00\n");
    fprintf(stdout, "  %s [options]\n\n", argv[0]);
    argEval_outputHelp(stdout);
    exit(0);
  }
  return (EXIT_SUCCESS);
}

if (0 != argEval_hasOptionBeenSet(yValue))
...
 */


