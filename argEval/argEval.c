/*******************************************************************************
 *  Copyright (c) 2012 Supercomputing Systems AG
 *  All rights reserved.
 *******************************************************************************/

#include "argEval.h"

typedef struct
{
  int32_t Identifier;
  int32_t Occurence;
  int32_t ParameterCount;
  char** Parameters;
} optionVariableHolder_t;

/**************************************************************************//**
*  @brief Looks for a value in the given array
*
******************************************************************************
*  @param[in]  array       The Array to look for the value
*  @param[in]  size        The size of the input array in elements.
*  @param[in]  value       The value to look for...
*
******************************************************************************
*  @return     int32_t     The return value is the count of the occurances
*                          of value within the array.
******************************************************************************/
int32_t _argEval_int32ArrayContainsValue(const int32_t* array, const size_t size, const int32_t value);

/**************************************************************************//**
*  @brief This Routine allocates a new option Array on the heap (this must be
*  freed after it is no longer used!) and copies the all elements, sorted by 
*  thelength of the option string into the new memory.
*
******************************************************************************
*  @param[in]  optionArray The Array to sort and copy
*  @param[in]  size        The size of the input array in elements.
*
******************************************************************************
*  @return     OptionDefinition_t*  The new array with the sorted elements.
******************************************************************************/
OptionDefinition_t* _argEval_getOptionsSortedByStringLen(const OptionDefinition_t* optionArray, const size_t size);

/**************************************************************************//**
*  @brief This Routine searches for the rigth option and gives back a pointer
*  to the fitting Option definition.
*
******************************************************************************
*  @param[in]  arg                  The option to look for
*
******************************************************************************
*  @return     OptionDefinition_t*  The pointer to the fitting option or NULL
*                                   if no option was found.
******************************************************************************/
OptionDefinition_t* argEval_convertStringToOption(const char* arg);

/**************************************************************************//**
*  @brief This Routine searches for the option Holder with the given
*  Identifier.
*
******************************************************************************
*  @param[in]  Identifier               The Identifier to look for
*
******************************************************************************
*  @return     optionVariableHolder_t*  The pointer to the fitting optionHolder
*                                       or NULL if no option Holder was found.
******************************************************************************/
optionVariableHolder_t* _argEval_findOptionHolderByIdentifier(const int32_t Identifier);

char* _argEval_findHelpStringByIdentifier(const int32_t Identifier);

#define _ARGEVAL_FREE_MEM(pointer) free(pointer)
#define _ARGEVAL_ALLOC_MEM(type, emelents) (type*)malloc(sizeof(type)*emelents)
#define _ARGEVAL_REALLOC_MEM(type, oldPointer, newEmelents) (type*)realloc(oldPointer,sizeof(type)*newEmelents)
#define _ARGEVAL_COPY_MEM(dest, source, size) memcpy(dest,source,size)
#define _ARGEVAL_STR_CMP(ref, value) strcmp(ref,value)

#ifdef DEBUG
#define _ARGEVAL_LOG(message, args...) \
{\
  printf("%s:%d:: ",__FUNCTION__,__LINE__ ); \
  printf(message, ##args); \
  printf("\n"); \
}
#else
#define _ARGEVAL_LOG(message, args...)
#endif

size_t _argEval_optionArraySize = 0;
OptionDefinition_t* _argEval_optionArray = NULL;
int32_t _argEval_parsedOptionArrayCount = 0;
optionVariableHolder_t* _argEval_parsedOptionArray = NULL;
int32_t _argEval_unrecognizedParameterCount = 0;
char** _argEval_unrecognizedParameters = NULL;

void argEval_registerOptionArray(const OptionDefinition_t* optionArray, const size_t size)
{
  _ARGEVAL_LOG("Got new option array (%d elements)", size);
  if (NULL != _argEval_optionArray)
  {
    _ARGEVAL_LOG("Freeing old sorted option array");
    _ARGEVAL_FREE_MEM(_argEval_optionArray);
    _argEval_optionArraySize = 0;
  }
  _ARGEVAL_LOG("Creating new sorted option array");
  _argEval_optionArray = _argEval_getOptionsSortedByStringLen(optionArray, size);
  _argEval_optionArraySize = size;
}

int32_t argEval_Parse(int argc, char** argv, FILE* errorOut)
{
  int32_t n, m;
  OptionDefinition_t* fittingOption;
  optionVariableHolder_t* fittingOptionHolder;

  _ARGEVAL_LOG("Start parsing of %d arguments", argc);
  if (NULL != _argEval_parsedOptionArray)
  {
    _ARGEVAL_LOG("Freeing old parsed options");
    for (n = 0; n < _argEval_parsedOptionArrayCount; n++)
    {
      if (NULL != _argEval_parsedOptionArray[n].Parameters)
      {
        _ARGEVAL_FREE_MEM(_argEval_parsedOptionArray[n].Parameters);
        _argEval_parsedOptionArray[n].Parameters = NULL;
        _argEval_parsedOptionArray[n].ParameterCount = 0;
      }
    }
    _ARGEVAL_FREE_MEM(_argEval_parsedOptionArray);
    _argEval_parsedOptionArray = NULL;
    _argEval_parsedOptionArrayCount = 0;
  }

  if (NULL != _argEval_unrecognizedParameters)
  {
    _ARGEVAL_LOG("Freeing old parsed loose parameters");
    _ARGEVAL_FREE_MEM(_argEval_unrecognizedParameters);
    _argEval_unrecognizedParameters = NULL;
    _argEval_unrecognizedParameterCount = 0;
  }

  if ((1 >= argc) || (0 >= _argEval_optionArraySize))
  {
    if (1 < argc)
    {
      _argEval_unrecognizedParameters = _ARGEVAL_ALLOC_MEM(char*, argc);
      for (n = 1; n < argc; n++)
      {
        _argEval_unrecognizedParameters[_argEval_unrecognizedParameterCount] = argv[n];
        _argEval_unrecognizedParameterCount++;
      }
    }
    _ARGEVAL_LOG("Paring done; got %d loose Parameters", _argEval_unrecognizedParameterCount);
    _ARGEVAL_LOG("Paring done; got %d Options", _argEval_parsedOptionArrayCount);
    return _argEval_parsedOptionArrayCount;
  }

  _argEval_parsedOptionArray = _ARGEVAL_ALLOC_MEM(optionVariableHolder_t, argc);
  _argEval_unrecognizedParameters = _ARGEVAL_ALLOC_MEM(char*, argc);

  for (n = 1; n < argc; n++)
  {
    if (NULL != (fittingOption = argEval_convertStringToOption(argv[n])))
    {
      if (NULL == (fittingOptionHolder = _argEval_findOptionHolderByIdentifier(fittingOption->Identifier)))
      {
        fittingOptionHolder = &_argEval_parsedOptionArray[_argEval_parsedOptionArrayCount];
        fittingOptionHolder->ParameterCount = 0;
        fittingOptionHolder->Parameters = NULL;
        fittingOptionHolder->Occurence = 0;
        _argEval_parsedOptionArrayCount++;
      }
      fittingOptionHolder->Identifier = fittingOption->Identifier;
      fittingOptionHolder->Occurence++;
      if (0 != fittingOption->ParameterCount)
      {
        if (NULL == fittingOptionHolder->Parameters)
        {
          fittingOptionHolder->Parameters = _ARGEVAL_ALLOC_MEM(char*, fittingOption->ParameterCount);
        }
        else
        {
          fittingOptionHolder->Parameters = _ARGEVAL_REALLOC_MEM(char*, fittingOptionHolder->Parameters, fittingOptionHolder->ParameterCount + fittingOption->ParameterCount);
        }
        n++;
        for (m = 0; (m < fittingOption->ParameterCount) && (n < argc); m++)
        {
          fittingOptionHolder->Parameters[fittingOptionHolder->ParameterCount] = (char*) argv[n];
          fittingOptionHolder->ParameterCount++;
          n++;
        }
        n--;

        if (fittingOptionHolder->ParameterCount < (fittingOptionHolder->Occurence * fittingOption->ParameterCount))
        {
          fprintf(errorOut, "The option '%s' must have more parameters (%d):\n", fittingOption->OptionString, fittingOption->ParameterCount);
          if (NULL != fittingOption->Description)
          {
            fprintf(errorOut, "%s\n", fittingOption->Description);
          }
          else
          {
            fprintf(errorOut, "%s\n", _argEval_findHelpStringByIdentifier(fittingOption->Identifier));
          }
        }
      }
    }
    else
    {
      _argEval_unrecognizedParameters[_argEval_unrecognizedParameterCount] = argv[n];
      _argEval_unrecognizedParameterCount++;
      if (errorOut != NULL)
      {
        fprintf(errorOut, "Found unreconizable Option: %s\n", argv[n]);
      }
    }
  }

  _ARGEVAL_LOG("Paring done; got %d loose Parameters", _argEval_unrecognizedParameterCount);
  _ARGEVAL_LOG("Paring done; got %d Options", _argEval_parsedOptionArrayCount);
  return _argEval_parsedOptionArrayCount;
}

int32_t argEval_hasOptionBeenSet(const int32_t Identifier)
{
  optionVariableHolder_t* Holder = _argEval_findOptionHolderByIdentifier(Identifier);
  if (NULL != Holder)
  {
    return Holder->Occurence;
  }
  return 0;
}

int32_t argEval_giveLooseParameterCount(void)
{
  return _argEval_unrecognizedParameterCount;
}

char** argEval_giveLooseParameters(void)
{
  return _argEval_unrecognizedParameters;
}

int32_t argEval_giveOptionParameterCount(const int32_t Identifier)
{
  optionVariableHolder_t* Holder = _argEval_findOptionHolderByIdentifier(Identifier);
  if (NULL != Holder)
  {
    return Holder->ParameterCount;
  }
  return 0;
}

char** argEval_giveOptionParameters(const int32_t Identifier)
{
  optionVariableHolder_t* Holder = _argEval_findOptionHolderByIdentifier(Identifier);
  if (NULL != Holder)
  {
    return Holder->Parameters;
  }
  return NULL;
}

void argEval_outputHelp(FILE* outputStream)
{
  uint32_t n;
  int32_t outputedIdentifiers[_argEval_optionArraySize];
  int32_t outputedIdentifiersCount = 0;

  memset(outputedIdentifiers, 0, sizeof (outputedIdentifiers));

  for (n = 0; n < _argEval_optionArraySize; n++)
  {
    if (0 == _argEval_int32ArrayContainsValue(outputedIdentifiers, outputedIdentifiersCount, _argEval_optionArray[n].Identifier))
    {
      if (NULL != _argEval_optionArray[n].Description)
      {
        fprintf(outputStream, "%s\n", _argEval_optionArray[n].Description);
        outputedIdentifiers[outputedIdentifiersCount] = _argEval_optionArray[n].Identifier;
        outputedIdentifiersCount++;
      }
    }
  }
}

OptionDefinition_t* argEval_convertStringToOption(const char* arg)
{
  uint32_t n;

  for (n = 0; n < _argEval_optionArraySize; n++)
  {
    if (0 == _ARGEVAL_STR_CMP(_argEval_optionArray[n].OptionString, arg))
    {
      return &_argEval_optionArray[n];
    }
  }
  return NULL;
}

char* _argEval_findHelpStringByIdentifier(const int32_t Identifier)
{
  uint32_t n;

  for (n = 0; n < _argEval_optionArraySize; n++)
  {
    if (_argEval_optionArray[n].Identifier == Identifier)
    {
      if (NULL != _argEval_optionArray[n].Description)
      {
        return _argEval_optionArray[n].Description;
      }
    }
  }
  return NULL;
}

optionVariableHolder_t* _argEval_findOptionHolderByIdentifier(const int32_t Identifier)
{
  int32_t n;

  for (n = 0; n < _argEval_parsedOptionArrayCount; n++)
  {
    if (_argEval_parsedOptionArray[n].Identifier == Identifier)
    {
      return &_argEval_parsedOptionArray[n];
    }
  }
  return NULL;
}

OptionDefinition_t* _argEval_getOptionsSortedByStringLen(const OptionDefinition_t* optionArray, const size_t size)
{
  uint32_t m, n, max;
  int32_t strLength[size];
  OptionDefinition_t* ret = _ARGEVAL_ALLOC_MEM(OptionDefinition_t, size);

  for (n = 0; n < size; n++)
  {
    strLength[n] = strlen(optionArray[n].OptionString);
  }

  _ARGEVAL_COPY_MEM(ret, optionArray, sizeof (OptionDefinition_t) * size);

  for (n = 0; n < size - 1; n++)
  {
    max = n;
    for (m = n + 1; m < size; m++)
    {
      if (strLength[m] > strLength[max])
      {
        max = m;
      }
    }
    if (max != n)
    {
      int32_t len_temp = strLength[n];
      OptionDefinition_t OptionTemp;
      //COPY_MEM(dest, source, size)
      _ARGEVAL_COPY_MEM(&OptionTemp, &ret[n], sizeof (OptionDefinition_t));
      strLength[n] = strLength[max];
      _ARGEVAL_COPY_MEM(&ret[n], &ret[max], sizeof (OptionDefinition_t));
      strLength[max] = len_temp;
      _ARGEVAL_COPY_MEM(&ret[max], &OptionTemp, sizeof (OptionDefinition_t));
    }
  }

  return ret;
}

int32_t _argEval_int32ArrayContainsValue(const int32_t* array, const size_t size, const int32_t value)
{
  uint32_t n;
  int32_t ret = 0;
  for (n = 0; n < size; n++)
  {
    if (array[n] == value)
    {
      ret++;
    }
  }
  return ret;
}


