/* 
 * File:   fastmake.c
 * Author: gandhi
 *
 * Created on 1. September 2013, 17:13
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <dirent.h>

#include "argEval/argEval.h"

#define _FREE_MEM(pointer) free(pointer)
#define _ALLOC_MEM(type, emelents) (type*)malloc(sizeof(type)*(emelents))
#define _REALLOC_MEM(type, oldPointer, newEmelents) (type*)realloc(oldPointer,sizeof(type)*(newEmelents))

typedef enum
{
  postBuildCommand,
  showHelp,
  verbose,
  configFileName,
  makeLib,
  compiler,
  linker,
  makeClean,
  outputEnding,
} optionEnum;

OptionDefinition_t optionArray[] = {
  {showHelp,         0, "-h",          NULL},
  {showHelp,         0, "--help",      "  --help, -h          Shows the help (this screen)"},
  {verbose,          0, "-V",          "! -V                  Be verbose (show debug messages)"},
  {makeClean,        0, "-c",          "! -c                  deletes all .o files"},
  {configFileName,   1, "--config",    "  --config            use the specified config file (default: makeconfig)"},
  {makeLib,          0, "-lib",        "! -lib                makes a libriry file (the files to compile must be specified)"},
  {compiler,         1, "-t",          NULL},
  {compiler,         1, "--compiler",  "  --compiler, -t      use the specified compiler (default: gcc)"},
  {linker,           1, "-l",          NULL},
  {linker,           1, "--linker",    "  --linker, -l        use the specified linker (default: gcc)"},
  {outputEnding,     1, "-e",          NULL},
  {outputEnding,     1, "--ending",    "  --ending, -e        specifies the ending for the built file (for example: .efl) (default: nothing)"},
  {postBuildCommand, 1, "-p",          NULL},
  {postBuildCommand, 1, "--postBuild", "! --postBuild, -p     specifies an Command that should get executed when the build has finished.\n"
                                       "                      it is possible to use variables to get information about the currently finished build:\n"
				        "                         output         the output filename (including fileending) of the build.\n\n"
				        "                      For Example: ... -p \"objdump -Obinary $output output.bin\"" }
};

const char searchFileEnding[]=".c";
const int searchFileEndingLen=sizeof(searchFileEnding)-1;

const char searchStartDir[]=".";

const char searchFunctionName[]="main";
const int searchFunctionNameLen=sizeof(searchFunctionName)-1;

const char* allowedMainFormats[]={ "intmain(intargc,char**argv)",
                                   "intmain(void)",
                                   "intmain()",
                                   "voidmain(void)",
                                   "voidmain()" };
const int allowedMainFormatsCount=sizeof(allowedMainFormats)/sizeof(char*);

const char searchIncludeName[]="#include";
const char specificIncludeName[]="#include\"";

#define COMPILER_CONFIG_KEY		"Compiler"
char* compileTool="gcc";
#define COMPILER_PARAMETER_CONFIG_KEY	"Default-Compiler-Parameter"
char* compileToolParameter="-O0 -g -Wall -Wextra";

#define LINKER_CONFIG_KEY		"Linker"
char* linkerTool="gcc";
#define LINKER_PARAMETER_CONFIG_KEY	"Default-Linker-Parameter"
char* linkerToolParameter="--static";

#define OUTPUT_FILE_ENDING_KEY		"Output-File-Ending"
char* outputFileEnding="";

/* Configuration */
/*****************/
char* makeConfigFile="makeconfig";

/*****************/

typedef struct fileTreeItem_t fileTreeItem_t;

struct fileTreeItem_t{
  char* hFile;
  char* cFile;
  char* oFile;
};

fileTreeItem_t* compileFiles=NULL;
int compileFilesCount=0;
int compileFilesCapacity=0;

typedef struct CompilerParameter_t CompilerParameter_t;

struct CompilerParameter_t{
  char* cFile;
  char* parameter;
};

CompilerParameter_t* compileParameters=NULL;
int compileParametersCount=0;
int compileParametersCapacity=0;

char* stringCopy(const char* src){
  int len=strlen(src)+1;
  char* ret=_ALLOC_MEM(char, len);
  memcpy(ret, src, len);
  return ret;
}

fileTreeItem_t* findHFile(const char* filename){
  char* nameStart=strrchr(filename, '/');
  int nameLen, n, itemLen;
  
  if(nameStart==NULL){
    nameStart=strrchr(filename, '\\');
  }
  
  if(nameStart==NULL){
    nameStart=(char*)filename;
  }else{
    nameStart=&nameStart[1];
  }
  nameLen=strlen(nameStart);
  
  for(n=0; n<compileFilesCount; n++){
    if(compileFiles[n].hFile!=NULL){
      itemLen=strlen(compileFiles[n].hFile);
      if(nameLen<=itemLen){
	if(0==strcmp(nameStart, &compileFiles[n].hFile[itemLen-nameLen])){
	  return &compileFiles[n];
	}
      }
    }
  }
  return NULL;
}

fileTreeItem_t* addFile(const char* hFilename, const char* cFilename){
  fileTreeItem_t* ret;
  
  if(compileFilesCount==compileFilesCapacity){
    compileFilesCapacity+=16;
    compileFiles=_REALLOC_MEM(fileTreeItem_t, compileFiles, compileFilesCapacity);
    
    if(compileFiles==NULL){
      fprintf(stderr,"MemoryAllocation failed! (line: %d)\n", __LINE__);
      exit(-1);
    }
    
    memset(&compileFiles[compileFilesCapacity-16], 0, 16*sizeof(fileTreeItem_t));
  }
  
  if(hFilename!=NULL){
    ret=findHFile(hFilename);
    if(ret!=NULL){
      if(cFilename!=NULL){
	if(ret->cFile!=NULL){
	  free(ret->cFile);
	}
	ret->cFile=stringCopy(cFilename);
      }
      return ret;
    }
  }
  
  ret=&compileFiles[compileFilesCount];
  compileFilesCount++;
  
  if(ret->hFile!=NULL){
    free(ret->hFile);
    ret->hFile=NULL;
  }
  
  if(hFilename!=NULL){
    ret->hFile=stringCopy(hFilename);
  }
  
  if(ret->cFile!=NULL){
    free(ret->cFile);
    ret->cFile=NULL;
  }
  
  if(cFilename!=NULL){
    ret->cFile=stringCopy(cFilename);
  }
  
  if(ret->oFile!=NULL){
    free(ret->oFile);
    ret->oFile=NULL;
  }
  
  return ret;
}

void clearList(void){
  compileFilesCount=0;
}

void freeList(void){
  int n;
  
  clearList();
  
  for(n=0;n<compileFilesCapacity;n++){
    if(compileFiles[n].cFile!=NULL){
      free(compileFiles[n].cFile);
    }
    if(compileFiles[n].hFile!=NULL){
      free(compileFiles[n].hFile);
    }
    if(compileFiles[n].oFile!=NULL){
      free(compileFiles[n].oFile);
    }
  }
  
  compileFilesCapacity=0;
  free(compileFiles);
  compileFiles=NULL;
}

CompilerParameter_t* findCompileParameter(const char* filename){
  int n;
  
  for(n=0;n<compileParametersCount;n++){
    if(0==strcmp(compileParameters[n].cFile, filename)){
      return &compileParameters[n];
    }
  }
  
  return NULL;
}

CompilerParameter_t* addParameter(const char* filename, const char* parameter){
  CompilerParameter_t* ret;
  
  if(compileParametersCount==compileParametersCapacity){
    compileParametersCapacity+=16;
    compileParameters=_REALLOC_MEM(CompilerParameter_t, compileParameters, compileParametersCapacity);
    
    if(compileParameters==NULL){
      fprintf(stderr,"MemoryAllocation failed! (line: %d)\n", __LINE__);
      exit(-1);
    }
    
    memset(&compileParameters[compileParametersCapacity-16], 0, 16*sizeof(CompilerParameter_t));
  }
  
  ret=findCompileParameter(filename);
  if(ret!=NULL){
    if(parameter!=NULL){
      if(ret->parameter!=NULL){
	free(ret->parameter);
      }
      ret->parameter=stringCopy(parameter);
    }
    return ret;
  }
  
  ret=&compileParameters[compileParametersCount];
  compileParametersCount++;
  
  if(ret->cFile!=NULL){
    free(ret->cFile);
    ret->cFile=NULL;
  }
  
  if(ret->parameter!=NULL){
    free(ret->parameter);
    ret->parameter=NULL;
  }
  
  if(filename!=NULL){
    ret->cFile=stringCopy(filename);
  }
  
  if(parameter!=NULL){
    ret->parameter=stringCopy(parameter);
  }
  
  return ret;
}

void clearParameterList(void){
  compileParametersCount=0;
}

void freeParameterList(void){
  int n;
  
  for(n=0;n<compileParametersCapacity;n++){
    if(compileParameters[n].cFile!=NULL){
      free(compileParameters[n].cFile);
    }
    if(compileParameters[n].parameter!=NULL){
      free(compileParameters[n].parameter);
    }
  }
  
  free(compileParameters);
  compileParameters=NULL;
  compileParametersCapacity=0;
  compileParametersCount=0;
}

void strRemoveSpaces(char* str){
  int n;
 
  for(n=0;n<((int)strlen(str));n++){
    if(str[n] == ' '){
      memmove(&str[n], &str[n+1], strlen(str)-n);
      n--;
    }
  }
}

int checkForMain(const char* filename){
  FILE* inFile;
  int ret=-1;
  int lineCount=0;
  int n;
  char* line = NULL;
  size_t len = 0;
  ssize_t read;
  
  inFile = fopen (filename,"r");
  if (inFile==NULL)
  {
    return ret;
  }
  
  while (((read = getline(&line, &len, inFile)) != -1) && ret < 0) {
    if(NULL != strstr(line, searchFunctionName)){
      strRemoveSpaces(line);
      
      for(n=0;n<allowedMainFormatsCount;n++){
	if(0 == strncmp(allowedMainFormats[n], line, strlen(allowedMainFormats[n]))){
	  ret=lineCount;
	  printf("found %s on line %d in file %s: %s", searchFunctionName, lineCount, filename, line);
	}
      }
    }
    lineCount++;
  }

  free(line);
  fclose (inFile);
  
  return ret;
}

char* straddNoSpace(char* destination, int* destLen, const char* source){
  int destStrLen=0;
  int sourceLen=0;
  
  if(destination!=NULL){
    destStrLen=strlen(destination);
  }
  if(source!=NULL){
    sourceLen=strlen(source);
  }
  
  if((destStrLen+sourceLen+2)>*destLen){
    destination=_REALLOC_MEM(char, destination, destStrLen+sourceLen+2);
    
    if(destination==NULL){
      fprintf(stderr,"MemoryAllocation failed! (line: %d)\n", __LINE__);
      exit(-1);
    }
    *destLen=destStrLen+sourceLen+2;
  }
  
  if(destStrLen==0){
    destination[0]='\0';
  }
  if(sourceLen!=0){
    strcat(destination, source);
  }
  
  return destination;
}

char* findCFile(const char* refCFile, const char* hFilename){
  FILE* exist;
  char* refPath=stringCopy(refCFile);
  int refPathLen=strlen(refPath);
  char* fileEnding;
  char* refPathSign=strrchr(refPath,'/');
  
  if(refPathSign==NULL){
    refPathSign=strrchr(refPath,'\\');
  }
  
  if(refPathSign==NULL){
    refPath[0]='\0';
  }else{
    refPathSign[1]='\0';
  }
  
  refPath=straddNoSpace(refPath, &refPathLen, hFilename);
  fileEnding=strrchr(refPath,'.');
  
  if(fileEnding==NULL){
    free(refPath);
    return NULL;
  }
  
  fileEnding[1]='c';
  fileEnding[2]='\0';
  
  exist=fopen(refPath,"r");
  if(exist==NULL){
    free(refPath);
    return NULL;
  }
  
  fclose(exist);
  return refPath;
}

void searchDependencies(const char* filename){
  FILE* inFile;
  char* line = NULL;
  char* filePointer;
  char* fileEnd;
  size_t len = 0;
  ssize_t read;
  fileTreeItem_t* currentFile;
  
  inFile = fopen (filename,"r");
  if (inFile==NULL)
  {
    return;
  }
  
  while ((read = getline(&line, &len, inFile)) != -1) {
    if(NULL != strstr(line, searchIncludeName)){
      strRemoveSpaces(line);
      if(NULL != strstr(line, specificIncludeName)){
	filePointer=strchr(line, '\"');
	if(filePointer!=NULL){
	  filePointer=&filePointer[1];
	  fileEnd=strchr(filePointer, '\"');
	  if(fileEnd!=NULL){
	    fileEnd[0]='\0';
	  }
	  printf("found Dependency %s in File %s", filePointer, filename);
	  currentFile=addFile(filePointer, NULL);
	  
	  if(currentFile->cFile==NULL){
	    currentFile->cFile=findCFile(filename, filePointer);
	    if(currentFile->cFile==NULL){
	      printf(" <- warning: no c-File found\n");
	    }else{
	      printf("\n");
	      searchDependencies(currentFile->cFile);
	    }
	  }else{
	    printf(" <- already added\n");
	  }
	}
      }
    }
  }

  free(line);
  fclose (inFile);
}

char* stradd(char* destination, int* destLen, const char* source){
  int destStrLen=0;
  int sourceLen=0;
  
  if(destination!=NULL){
    destStrLen=strlen(destination);
  }
  if(source!=NULL){
    sourceLen=strlen(source);
  }
  
  if((destStrLen+sourceLen+2)>*destLen){
    destination=_REALLOC_MEM(char, destination, destStrLen+sourceLen+2);
    
    if(destination==NULL){
      fprintf(stderr,"MemoryAllocation failed! (line: %d)\n", __LINE__);
      exit(-1);
    }
    *destLen=destStrLen+sourceLen+2;
  }
  
  if(destStrLen!=0){
    strcat(destination, " ");
  }else{
    destination[0]='\0';
  }
  if(sourceLen!=0){
    strcat(destination, source);
  }
  
  return destination;
}

void compileAll(void){
  int n;
  char* oFile=NULL;
  char* fileEnding=NULL;
  char* cFile=NULL;
  char* command=NULL;
  int commandLen=0;
  CompilerParameter_t* options;
  char* parameter;
  
  for(n=0;n<compileFilesCount;n++){
    if(compileFiles[n].cFile!=NULL){
      cFile=strrchr(compileFiles[n].cFile, '/');
      if(cFile==NULL){
	cFile=strrchr(compileFiles[n].cFile, '\\');
      }
      if(cFile==NULL){
	cFile=compileFiles[n].cFile;
      }else{
	cFile=&cFile[1];
      }
      oFile=stringCopy(cFile);
      fileEnding=strrchr(oFile, '.');
      if(fileEnding!=NULL){
	fileEnding[1]='o';
	fileEnding[2]='\0';
	
	compileFiles[n].oFile=oFile;
	fileEnding=NULL;
	oFile=NULL;
	
	parameter=compileToolParameter;
	options=findCompileParameter(compileFiles[n].cFile);
	if(options!=NULL){
	  parameter=options->parameter;
	}
	
	command=stradd(command, &commandLen, compileTool);
	command=stradd(command, &commandLen, "-c");
	command=stradd(command, &commandLen, compileFiles[n].cFile);
	command=stradd(command, &commandLen, parameter);
	
	printf("Executing: %s\n", command);
	system(command);
	command[0]='\0';
      }
    }
  }
  free(command);
}

void linkApplication(void){
  int n;
  char* command=NULL;
  int commandLen=0;
  
  command=stradd(command, &commandLen, linkerTool);
  command=stradd(command, &commandLen, "-o");
  
  if(compileFilesCount>0){
    char* outFile=stringCopy(compileFiles[0].cFile);
    char* ending=strrchr(outFile, '.');
    ending[0]='\0';
    command=stradd(command, &commandLen, outFile);
    command=straddNoSpace(command, &commandLen, outputFileEnding);
    free(outFile);
  }
  
  for(n=0;n<compileFilesCount;n++){
    if(compileFiles[n].oFile!=NULL){
      command=stradd(command, &commandLen, compileFiles[n].oFile);
    }
  }
  
  command=stradd(command, &commandLen, linkerToolParameter);
  
  printf("Linking application: %s\n", command);
  system(command);
  
  free(command);
}

void buildFile(const char* filename){
  addFile(NULL, filename);
  
  searchDependencies(filename);
  
  compileAll();
  
  linkApplication();
  
  clearList();
}

char** searchFilesWithMain(int* count, const char* dir){
  DIR* maindir;
  struct dirent* dirFile;
  int fileNameLen;
  char** ret=NULL;
  
  *count=0;
  maindir=opendir(dir);
  
  if (maindir)
  {
    while ((dirFile = readdir(maindir)) != NULL)
    {
      fileNameLen=strlen(dirFile->d_name);
      if(0==strcmp(&dirFile->d_name[fileNameLen-searchFileEndingLen],searchFileEnding)){
	printf("searching for main() in %s\n", dirFile->d_name);
	if(0<=checkForMain(dirFile->d_name)){
	  ret=_REALLOC_MEM(char*, ret, (*count)+1);
    
	  if(ret==NULL){
	    fprintf(stderr,"MemoryAllocation failed! (line: %d)\n", __LINE__);
	    exit(-1);
	  }
	  ret[*count]=stringCopy(dirFile->d_name);
	  (*count)++;
	}
      }
    }

    closedir(maindir);
  }
  
  return ret;
}

char* trimStart(char* str){
  int len=strlen(str);
  int n;
  
  for(n=0;n<len;n++){
    if(str[n]!=' ' && str[n]!='\t' && str[n]!='\n' && str[n]!='\r' && str[n]!='='){
      return &str[n];
    }
  }
  return NULL;
}

char* removeNewLineEnding(char* str){
  int len=strlen(str);
  int n;
  
  for(n=len-1;n>=0;n--){
    if(str[n]=='\n' || str[n]=='\r'){
      str[n]='\0';
    }else{
      return str;
    }
  }
  return str;
}

char* splitParameter(char* str){
  int len=strlen(str);
  int n;
  
  for(n=0;n<len;n++){
    if(str[n]==' ' || str[n]=='\t' || str[n]=='\n' || str[n]=='\r' || str[n]=='='){
      str[n]='\0';
      return removeNewLineEnding(trimStart(&str[n+1]));
    }
  }
  return NULL;
}

void parseConfigFile(const char* filename){
  FILE* inFile;
  char* line = NULL;
  char* key;
  char* parameter;
  int keyLen = 0;
  size_t len = 0;
  ssize_t read;
  int handled = 0;
  
  char* compilerKey = COMPILER_CONFIG_KEY;
  int compilerKeyLen = strlen(compilerKey);
  char* compilerParameterKey = COMPILER_PARAMETER_CONFIG_KEY;
  int compilerParameterKeyLen = strlen(compilerParameterKey);
  
  char* linkerKey = LINKER_CONFIG_KEY;
  int linkerKeyLen = strlen(linkerKey);
  char* linkerParameterKey = LINKER_PARAMETER_CONFIG_KEY;
  int linkerParameterKeyLen = strlen(linkerParameterKey);
  
  char* endingKey = OUTPUT_FILE_ENDING_KEY;
  int endingKeyLen = strlen(endingKey);
  
  inFile = fopen(filename,"r");
  if (inFile == NULL)
  {
    return;
  }
  
  while ((read = getline(&line, &len, inFile)) != -1) {
    key = trimStart(line);
    handled = 0;
    if(key != NULL && key[0] != '#'){
      parameter = splitParameter(key);
      keyLen = strlen(key);
      
      printf("Found config: Key = \"%s\", Parameter \"%s\"", key, parameter);
      
      if(handled==0 && keyLen==compilerKeyLen && 0==strcmp(compilerKey, key)){
	compileTool=stringCopy(parameter);
	printf(" <- compiler command");
	handled = 1;
      }
      
      if(handled==0 && keyLen==linkerKeyLen && 0==strcmp(linkerKey, key)){
	linkerTool=stringCopy(parameter);
	printf(" <- linker command");
	handled = 1;
      }
      
      if(handled==0 && keyLen==compilerParameterKeyLen && 0==strcmp(compilerParameterKey, key)){
	char* found;
	compileToolParameter=stringCopy(parameter);
	printf(" <- compiler parameter");
	
	found=strstr(compileToolParameter, " -c");
	if(found!=NULL && (found[3]==' ' || found[3]=='\0' || found[3]=='\t')){
	  found[0]=' ';
	  found[1]=' ';
	  found[2]=' ';
	}
	
	handled = 1;
      }
      
      if(handled==0 && keyLen==linkerParameterKeyLen && 0==strcmp(linkerParameterKey, key)){
	char* found;
	linkerToolParameter=stringCopy(parameter);
	printf(" <- linker parameter");
	
	found=strstr(linkerToolParameter, " -o");
	if(found!=NULL && (found[3]==' ' || found[3]=='\0' || found[3]=='\t')){
	  found[0]=' ';
	  found[1]=' ';
	  found[2]=' ';
	}
	
	handled = 1;
      }
      
      if(handled==0 && keyLen==endingKeyLen && 0==strcmp(endingKey, key)){
	outputFileEnding=stringCopy(parameter);
	printf(" <- linker parameter");
	handled = 1;
      }
      
      if(handled==0 && keyLen > searchFileEndingLen && 0 == strcmp(searchFileEnding, &key[keyLen-searchFileEndingLen])){
	printf(" <- compiler options for c file");
	addParameter(key, parameter);
	handled = 1;
      }
      
      if(handled == 0){
	printf(" <- unknown"); 
      }
      
      printf("\n");
    }
  }

  free(line);
  fclose(inFile);
}

/*
 * 
 */
int main(int argc, char** argv) {
  int cFileCount;
  char** cFileList;
  int n;
  
  argEval_registerOptionArray(optionArray, sizeof (optionArray) / sizeof (OptionDefinition_t));
  argEval_Parse(argc, argv, NULL);

  if (0 != argEval_hasOptionBeenSet(showHelp)) {
    fprintf(stdout, "Fastmake V0.30\n");
    fprintf(stdout, "Copyright (C) 2013 CBInnovations\n");
    fprintf(stdout, "  %s [options]\n\n", argv[0]);
    argEval_outputHelp(stdout);
    exit(EXIT_SUCCESS);
  }
  
  if(0!=argEval_hasOptionBeenSet(configFileName)){
    makeConfigFile=argEval_giveOptionParameters(configFileName)[0];
  }
  parseConfigFile(makeConfigFile);
  
  if(0!=argEval_giveOptionParameterCount(compiler)){
    compileTool=argEval_giveOptionParameters(compiler)[0];
  }
  
  if(0!=argEval_giveOptionParameterCount(linker)){
    linkerTool=argEval_giveOptionParameters(linker)[0];
  }
  
  if(0!=argEval_giveOptionParameterCount(outputEnding)){
    outputFileEnding=argEval_giveOptionParameters(outputEnding)[0];
  }
  
  cFileCount=argEval_giveLooseParameterCount();
  if(cFileCount > 0){
    cFileList=argEval_giveLooseParameters();
  }else{
    if(0==argEval_hasOptionBeenSet(makeLib)){
      cFileList=searchFilesWithMain(&cFileCount, searchStartDir);
    }
  }
  
  for(n=0;n<cFileCount;n++){
    printf("Building: %s\n", cFileList[n]);
  }
  
  for(n=0;n<cFileCount;n++){
    buildFile(cFileList[n]);
    printf("\n");
  }
  
  if(0==argEval_giveLooseParameterCount()){
    for(n=0;n<cFileCount;n++){
      _FREE_MEM(cFileList[n]);
    }
    _FREE_MEM(cFileList);
  }
  
  freeParameterList();
  freeList();
  
  return (EXIT_SUCCESS);
}

